<?php

namespace App\Http\Controllers;

use App\Events\Chat\SendMessage;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;

class MessageController extends Controller
{

    public function index(User $user)
    {


        $userFrom = Auth::user()->id;
        $userTo = $user->id;

        /*
         * exibir somente as mensagem que usuario logado enviou para outro e as mensagens do outro p/  usuario logado
         */
        $messages = Message::where(
            function ($query) use ($userFrom, $userTo) {
                $query->where([
                    'from' => $userFrom,
                    'to' => $userTo
                ]);
            }
        )
            ->orWhere(
                function ($query) use ($userFrom, $userTo) {
                    $query->where([
                        'from' => $userTo,
                        'to' => $userFrom
                    ]);
                }
            )
            ->orderBy('created_at', 'ASC')->get();

        return ['messages' => $messages];
    }


    public function store(Request $request)
    {

        $message = new Message();

        $message->from = Auth::user()->id;

        $message->to = $request->to;

        $message->content = filter_var($request->content, FILTER_SANITIZE_STRIPPED);

        if ($request->hasFile('file') && $request->file('file')->isValid()) {

            $message->filePath = $request->file('file')->store("gzap-files/". Auth::user()->id);

            $message->fileOriginalName = $request->file('file')->getClientOriginalName();
        }

        $message->save();


        Event::dispatch(new SendMessage($message, $request->to));



        return ['ok' => $message];
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }




}
