<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    public function index(Request $request)
    {


        $query = request('query');

        $users = User::where('id', '!=', Auth::user()->id);

        if ($query) {
            $users->where('name', $query);
            $users->orWhere('email', $query);
            $users->orWhere('name', 'like', "%$query%");
        }

        return ['users' => $users->paginate(4)];

    }


    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (!filter_var($credentials['email'], FILTER_VALIDATE_EMAIL)) {
            return response()->json('Informe um e-mail válido', Response::HTTP_BAD_REQUEST);

        }

        $user = User::where('email', $credentials['email'])->first();

        if (!$user) {
            return response()->json('Usuário não encontrado', Response::HTTP_BAD_REQUEST);

        }

        if (Hash::check($credentials['password'], $user->password)) {
          //  dd($user->isOnline);
            $token = JWTAuth::fromUser($user);

            $user->isOnline = true;
            $user->save();
            $user['jwt'] = $token;

            return $user;
        }

        return response()->json('Sua senha esta incorreta', Response::HTTP_BAD_REQUEST);

    }


    public function register(Request $request)
    {
        $exitUser = User::where('email', $request->email)->first();

        if ($exitUser) {
            return response()->json('desculpe, esse e-mail já esta cadastrado na nossa base', Response::HTTP_BAD_REQUEST);

        }

        $data = $request->all();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            $data["image"] = $request->file('image')->store("perfils");

        }

        $data['password'] = Hash::make($request->password, [
            'rounds' => 12,
        ]);

        $user = User::create($data);

        return ['ok' => $user];
    }

    public function logout()
    {
        $user = Auth::user();
        $user->isOnline = false;
        $user->save();

        Auth::logout();


        return response()->json(['message' => 'success']);
    }


    public function getUser(Request $request)
    {

        $this->validate($request, [
            'token' => 'required'
        ]);


        $user = JWTAuth::authenticate($request->token);

        return response()->json(['user' => $user]);

    }


    public function show(User $user)
    {
        return response()->json(['user' => $user]);
    }


}
