<?php

use App\Http\Controllers\MessageController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('login', [UserController::class, 'login']);
Route::post('register', [UserController::class, 'register']);

Route::group(['middleware' => 'jwt.auth'], function () {

    Route::get('logout', [UserController::class, 'logout']);

    Route::post('user-info', [UserController::class, 'getUser']);

    Route::get('users', [UserController::class, 'index']);

    Route::get('users/{user}', [UserController::class, 'show']);


    Route::get('messages/{user}', [MessageController::class, 'index']);

    Route::post('messages/store', [MessageController::class, 'store']);


});
